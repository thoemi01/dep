# Thomas Hahn, 2020
#
# Project: cmake-deps
# File: Top level CMakeLists.txt file
# Description: Test installing of cmake projects.

#########################
# Preamble
#########################
cmake_minimum_required(VERSION 3.13)
project(dep VERSION 1.0.0)

#########################
# Project wide setup
#########################
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
add_compile_options(-Wall -Wextra)

# shared libs
set(BUILD_SHARED_LIBS ON)

# fetch cmake-files repository and add them to module path
message(STATUS "----------------------")
message(STATUS "Fetching cmake-files repository")
include(FetchContent)
FetchContent_Declare(cmakefiles
    GIT_REPOSITORY https://thoemi01@bitbucket.org/thoemi01/cmake-files.git
    GIT_TAG origin/master)
FetchContent_GetProperties(cmakefiles)
if(NOT cmakefiles_POPULATED)
    FetchContent_Populate(cmakefiles)
    list(APPEND CMAKE_MODULE_PATH ${cmakefiles_SOURCE_DIR}/cmake)
endif()
message(STATUS "Source directory: ${cmakefiles_SOURCE_DIR}")
message(STATUS "----------------------")

# general user options
option(INSTALL_DEP "Install dep library" OFF)

# set build and install directories
include(BuildAndInstallDirs)

# output config information
message(STATUS "###################################################################")
message(STATUS "Building Project dep")
message(STATUS "Build Type: ${CMAKE_BUILD_TYPE}")
message(STATUS "BUILD_SHARED_LIBS: ${BUILD_SHARED_LIBS}")
message(STATUS "INSTALL_DEP: ${INSTALL_DEP}")
message(STATUS "CMAKE_INSTALL_PREFIX: ${CMAKE_INSTALL_PREFIX}")

#########################
# Main build targets
#########################
add_subdirectory(src)

# output end of config information
message(STATUS "###################################################################")
